package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.portal.kernel.service.LayoutFriendlyURLLocalService;

public class FriendlyUrlLocalServiceTracker extends ServiceTracker<LayoutFriendlyURLLocalService,LayoutFriendlyURLLocalService> {
    public FriendlyUrlLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext,LayoutFriendlyURLLocalService.class, null);
    }
}
