package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import projectService.service.ProjectLocalService;

public class ProjectLocalServiceTracker extends ServiceTracker<ProjectLocalService, ProjectLocalService> {

    public ProjectLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, ProjectLocalService.class, null);
    }
}
