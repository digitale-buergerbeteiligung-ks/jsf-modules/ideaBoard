package beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import analysisService.model.TagData;
import servicetrackers.TagLocalServiceTracker;

@ManagedBean(name="tagService", eager = true)
public class TagService{

	private List<String> tags = new ArrayList<String> ();
	private List<String> tempTags = new ArrayList<String> ();
	
	private TagLocalServiceTracker tagLocalServiceTracker;
	
	@PostConstruct
	public void init(){
		
		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		BundleContext bundleContext = bundle.getBundleContext();
		tagLocalServiceTracker = new TagLocalServiceTracker(bundleContext);
		tagLocalServiceTracker.open();
		
		if(tagLocalServiceTracker.getService().getTagDatasCount()==0){
			tagLocalServiceTracker.getService().addNewTag("default");
		}
		
		List<TagData> tagData = (tagLocalServiceTracker.getService().getTagDatas(-1, -1));
		tagData.forEach((TagData)->tags.add(TagData.getTag()));
	}
	
	public List<String> getTags(){
		tags = new ArrayList<String>();
		List<TagData> tagData = (tagLocalServiceTracker.getService().getTagDatas(-1, -1));
		tagData.forEach((TagData)->tags.add(TagData.getTag()));
		return tags;
	}
	
	public void setTags(String newTag){
		if(!tagLocalServiceTracker.getService().findTagData(newTag)){
			tagLocalServiceTracker.getService().addNewTag(newTag);
		}
	}

	public List<String> getTempTags(){
		return tempTags;
	}

	public void setTempTags(String tempTags){
		this.tempTags.add(tempTags);
	}
	
	@PreDestroy
	public void preDestroy(){
		tagLocalServiceTracker.close();
	}
}