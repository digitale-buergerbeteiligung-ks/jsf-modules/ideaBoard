function submitStepTwoAndGoToStepOne() {
	var button = document.getElementById('_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:submitStepTwoFromTab');
	button.click();
}

function submitStepTwoAndGoToStepThree() {
	var button = document.getElementById('_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:submitStepTwo');
	button.click();
}

function submitStepThreeAndGoToStepTwo() {
	var button = document.getElementById('_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepThreeForm:submitStepThreeFromTab');
	button.click();
}

function submitStepThreeAndGoToStepOne() {
	var button = document.getElementById('_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepThreeForm:submitStepThreeFromTab2');
	button.click();
}


var canvas1 = this.__canvas1 = new fabric.Canvas('c1', {
	isDrawingMode: true
});

var canvas2 = this.__canvas2 = new fabric.Canvas('c2', {
	isDrawingMode: true
});

var canvas3 = this.__canvas3 = new fabric.Canvas('c3', {
	isDrawingMode: true
});

var activeCanvas = canvas1;
console.log("Active canvas:", activeCanvas);

function handleTabChange(index) {
	if(index == 0) {
		activeCanvas = canvas1;
		activeCanvas.freeDrawingBrush.color = drawingColorEl.value;
		activeCanvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
	} else if(index == 1) {
		activeCanvas = canvas2;
		activeCanvas.freeDrawingBrush.color = drawingColorEl.value;
		activeCanvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
	} else {
		activeCanvas = canvas3;
		activeCanvas.freeDrawingBrush.color = drawingColorEl.value;
		activeCanvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
	}
}

(function () {
	var $ =
		function (id) {
			return document.getElementById(id);
		};

	var canvasContainer1 = $('canvas-container1');
	var canvasContainer2 = $('canvas-container2');
	var canvasContainer3 = $('canvas-container3');

	var canvasContainers = [canvasContainer1, canvasContainer2, canvasContainer3];
	var canvases = [canvas1, canvas2, canvas3];

	var fonts = ["arial black", "Helvetica", "Pacifico"];
	var fontSelect = $('font-family-select');
	fonts.forEach(function (font) {
		var option = document.createElement('option');
		option.innerHTML = font;
		option.value = font;
		fontSelect.appendChild(option);
	});


	// If a user edits an idea load the image from the given url
	var imageUrl1 = $("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgUrl1").value;
	if (imageUrl1 !== "/") {
		fabric.Image.fromURL(imageUrl1, function (img) {
			img.set({
				width: canvas1.width,
				height: canvas1.height
			});
			canvas1.add(img).renderAll().setActiveObject(img);
		});
	}
	
	
	// If a user edits an idea load the image from the given url
	var imageUrl2 = $("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgUrl2").value;
	if (imageUrl2 !== "/") {
		fabric.Image.fromURL(imageUrl2, function (img) {
			img.set({
				width: canvas2.width,
				height: canvas2.height
			});
			canvas2.add(img).renderAll().setActiveObject(img);
		});
	}
	
	// If a user edits an idea load the image from the given url
	var imageUrl3 = $("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgUrl3").value;
	if (imageUrl3 !== "/") {
		fabric.Image.fromURL(imageUrl3, function (img) {
			img.set({
				width: canvas3.width,
				height: canvas3.height
			});
			canvas3.add(img).renderAll().setActiveObject(img);
		});
	}

	
	function isCanvasBlank(canvasElement) {
		var blank = document.createElement('canvas');
		blank.width = canvasElement.width;
		blank.height = canvasElement.height;
		return canvasElement.toDataURL() == blank.toDataURL();
	}

	
	function handleDragStart(e) {
		[].forEach.call(images, function (img) {
			img.classList.remove('img_dragging');
		});
		this.classList.add('img_dragging');
	}

	function handleDragOver(e) {
		if (e.preventDefault) {
			e.preventDefault();
		}

		e.dataTransfer.dropEffect = 'copy';
		return false;
	}

	function handleDragEnter(e) {
		this.classList.add('over');
	}

	function handleDragLeave(e) {
		this.classList.remove('over');
	}

	function handleDrop(e) {
		e.preventDefault();

		var img = document.querySelector('#images img.img_dragging');

		var newImage = new fabric.Image(img, {
			width: img.width,
			height: img.height,
			// Set the center of the new object based on the event coordinates
			// relative
			// to the canvas container.
			left: e.layerX,
			top: e.layerY
		});
		newImage.hasControls = newImage.hasBorders = true;
		activeCanvas.add(newImage);

		return false;
	}

	function handleDragEnd(e) {
		// this/e.target is the source node.
		[].forEach.call(images, function (img) {
			img.classList.remove('img_dragging');
		});
	}

	if (dragAndDropSupported() === true) {
		var images = document.querySelectorAll('#images img');
		[].forEach.call(images, function (img) {
			img.addEventListener('dragstart', handleDragStart, false);
			img.addEventListener('dragend', handleDragEnd, false);
		});

		[].forEach.call(canvasContainers, function (cont) {
			cont.addEventListener('dragenter', handleDragEnter, false);
			cont.addEventListener('dragover', handleDragOver, false);
			cont.addEventListener('dragleave', handleDragLeave, false);
			cont.addEventListener('drop', handleDrop, false);
		});

	} else {
		alert("Ihr Browser unterstützt leider kein 'Drag and Drop'");
	}

	function dragAndDropSupported() {
		return 'draggable' in document.createElement('span');
	}

	fabric.Object.prototype.transparentCorners = false;

	var selectToolButton = $('select-tool');
	selectToolButton.onclick = function () {
		activeCanvas.isDrawingMode = false; // NO drawing mode
		$('select-option').style.display = 'none';
		$('drawing-mode-options').style.display = 'none';
		$('images').style.display = 'none';
		$('text-option').style.display = 'none';
		$('delete-option').style.display = 'none';
	}

	var editToolButton = $('edit-tool');
	editToolButton.onclick = function () {
		activeCanvas.isDrawingMode = true; // set to drawing mode
		$('select-option').style.display = 'none';
		$('drawing-mode-options').style.display = 'inline-block';
		$('images').style.display = 'none';
		$('text-option').style.display = 'none';
		$('delete-option').style.display = 'none';
	}

	var imagesToolButton = $('images-tool');
	imagesToolButton.onclick = function () {
		activeCanvas.isDrawingMode = false; // NO drawing mode
		$('select-option').style.display = 'none';
		$('drawing-mode-options').style.display = 'none';
		$('images').style.display = 'inline-block';
		$('text-option').style.display = 'none';
		$('delete-option').style.display = 'none';
	}

	var textToolButton = $('text-tool');
	textToolButton.onclick = function () {
		activeCanvas.isDrawingMode = false; // NO to drawing mode
		$('select-option').style.display = 'none';
		$('drawing-mode-options').style.display = 'none';
		$('images').style.display = 'none';
		$('text-option').style.display = 'inline-block';
		$('delete-option').style.display = 'none';
	}

	var uploadFileButton = $('_JSFIdeaBoard_WAR_JSFIdeaBoard_:imgUpload');
	
	var deleteElement = $('delete-element');
		deleteElement.onclick = function () {
		
		activeCanvas.getActiveObjects().forEach((obj) => { activeCanvas.remove(obj) });
		activeCanvas.discardActiveObject().renderAll();
			
		activeCanvas.isDrawingMode = false; // NO to drawing mode
		$('select-option').style.display = 'none';
		$('drawing-mode-options').style.display = 'none';
		$('images').style.display = 'none';
		$('text-option').style.display = 'none';
		$('delete-option').style.display = 'none';
	}
	
		var canvasWrapper1 = $('canvas-container1');
		canvasWrapper1.tabIndex = 1000;
		canvasWrapper1.addEventListener("keydown", myfunc, false);
		
		var canvasWrapper2 = $('canvas-container2');
		canvasWrapper2.tabIndex = 1000;
		canvasWrapper2.addEventListener("keydown", myfunc, false);
		
		var canvasWrapper3 = $('canvas-container3');
		canvasWrapper3.tabIndex = 1000;
		canvasWrapper3.addEventListener("keydown", myfunc, false);

		function myfunc(e){
	        if(e.keyCode == 8 || e.keyCode == 46) {
			activeCanvas.getActiveObjects().forEach((obj) => { activeCanvas.remove(obj) });
			activeCanvas.discardActiveObject().renderAll();
	        }
		}
	drawingColorEl = $('drawing-color'),
		drawingLineWidthEl = $('drawing-line-width'),

		textWrapper = $('text-wrapper'),
		addTextButton = $('add-text-button');

	var canvas1ModifiedCallback = function () {
		if (!fabric.Canvas.supports('toDataURL')) {
			alert('This browser doesn\'t provide means to serialize canvas to an image');
		} else {
			if(isCanvasBlank(canvas1)) {
				document.getElementById("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgData1").value = "-1";
			} else {
				document.getElementById("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgData1").value = canvas1.toDataURL('png');
			}
		}
	};

	canvas1.on('object:added', canvas1ModifiedCallback);
	canvas1.on('object:removed', canvas1ModifiedCallback);
	canvas1.on('object:modified', canvas1ModifiedCallback);

	var canvas2ModifiedCallback = function () {
		if (!fabric.Canvas.supports('toDataURL')) {
			alert('This browser doesn\'t provide means to serialize canvas to an image');
		} else {
			if(isCanvasBlank(canvas2)) {
				document.getElementById("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgData2").value = "-1";
			} else {
				document.getElementById("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgData2").value = canvas2.toDataURL('png');
			}
		}
	};
	
	canvas2.on('object:added', canvas2ModifiedCallback);
	canvas2.on('object:removed', canvas2ModifiedCallback);
	canvas2.on('object:modified', canvas2ModifiedCallback);
	
	var canvas3ModifiedCallback = function () {
		if (!fabric.Canvas.supports('toDataURL')) {
			alert('This browser doesn\'t provide means to serialize canvas to an image');
		} else {
			if(isCanvasBlank(canvas3)) {
				document.getElementById("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgData3").value = "-1";
			} else {
				document.getElementById("_JSFIdeaBoard_WAR_JSFIdeaBoard_:ideaInputStepTwoFormFooter:imgData3").value = canvas3.toDataURL('png');
			}
		}
	};
	
	canvas3.on('object:added', canvas3ModifiedCallback);
	canvas3.on('object:removed', canvas3ModifiedCallback);
	canvas3.on('object:modified', canvas3ModifiedCallback);
	
	uploadFileButton.onchange = function handleImage(e) {
		var reader = new FileReader();
		reader.onload = function (event) {

			if (e.target.files[0].size > 2097152) {
				alert("Die maximale Größe eines Bildes ist auf 2MB beschränkt. Bitte wähle ein anderes Bild aus.");
				return;
			};

			var imgObj = new Image();
			imgObj.src = event.target.result;
			imgObj.onload = function () {
				var image = new fabric.Image(imgObj);
				image.set({
					angle: 0,
					padding: 5,
					cornersize: 10
				});
				image.scaleToWidth(activeCanvas.getWidth());
				activeCanvas.centerObject(image);
				activeCanvas.add(image);
				activeCanvas.renderAll();
			};
		};
		reader.readAsDataURL(e.target.files[0]);
	};


	if (fabric.PatternBrush) {
		var vLinePatternBrush = new fabric.PatternBrush(activeCanvas);
		vLinePatternBrush.getPatternSrc = function () {

			var patternCanvas = fabric.document.createElement('canvas');
			patternCanvas.width = patternCanvas.height = 10;
			var ctx = patternCanvas.getContext('2d');

			ctx.strokeStyle = this.color;
			ctx.lineWidth = 5;
			ctx.beginPath();
			ctx.moveTo(0, 5);
			ctx.lineTo(10, 5);
			ctx.closePath();
			ctx.stroke();

			return patternCanvas;
		};

		var hLinePatternBrush = new fabric.PatternBrush(activeCanvas);
		hLinePatternBrush.getPatternSrc = function () {

			var patternCanvas = fabric.document.createElement('canvas');
			patternCanvas.width = patternCanvas.height = 10;
			var ctx = patternCanvas.getContext('2d');

			ctx.strokeStyle = this.color;
			ctx.lineWidth = 5;
			ctx.beginPath();
			ctx.moveTo(5, 0);
			ctx.lineTo(5, 10);
			ctx.closePath();
			ctx.stroke();

			return patternCanvas;
		};

		var squarePatternBrush = new fabric.PatternBrush(activeCanvas);
		squarePatternBrush.getPatternSrc = function () {

			var squareWidth = 10,
				squareDistance = 2;

			var patternCanvas = fabric.document.createElement('canvas');
			patternCanvas.width = patternCanvas.height = squareWidth + squareDistance;
			var ctx = patternCanvas.getContext('2d');

			ctx.fillStyle = this.color;
			ctx.fillRect(0, 0, squareWidth, squareWidth);

			return patternCanvas;
		};

		var diamondPatternBrush = new fabric.PatternBrush(activeCanvas);
		diamondPatternBrush.getPatternSrc = function () {

			var squareWidth = 10,
				squareDistance = 5;
			var patternCanvas = fabric.document.createElement('canvas');
			var rect = new fabric.Rect({
				width: squareWidth,
				height: squareWidth,
				angle: 45,
				fill: this.color
			});

			var canvasWidth = rect.getBoundingRect().width;

			patternCanvas.width = patternCanvas.height = canvasWidth + squareDistance;
			rect.set({
				left: canvasWidth / 2,
				top: canvasWidth / 2
			});

			var ctx = patternCanvas.getContext('2d');
			rect.render(ctx);

			return patternCanvas;
		};

	}

	$('drawing-mode-selector').onchange = function () {

		if (this.value === 'Bleistift') {
			var mode = "Pencil";
			activeCanvas.freeDrawingBrush = new fabric[mode + 'Brush'](activeCanvas);
		} else if (this.value === 'Muster') {
			activeCanvas.freeDrawingBrush = squarePatternBrush;
		} else if (this.value === 'Spray') {
			activeCanvas.freeDrawingBrush = new fabric[this.value + 'Brush'](activeCanvas);
		} else if (this.value === 'Kreis') {
			var mode = "Pattern";
			activeCanvas.freeDrawingBrush = new fabric[mode + 'Brush'](activeCanvas);
		} else {
			var mode = "Pencil";
			activeCanvas.freeDrawingBrush = new fabric[mode + 'Brush'](activeCanvas);
		}

		if (activeCanvas.freeDrawingBrush) {
			activeCanvas.freeDrawingBrush.color = drawingColorEl.value;
			activeCanvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
		}
	};

	drawingColorEl.onchange = function () {
		activeCanvas.freeDrawingBrush.color = this.value;
	};

	drawingLineWidthEl.onchange = function () {
		activeCanvas.freeDrawingBrush.width = parseInt(this.value, 10) || 1;
	};


	if (activeCanvas.freeDrawingBrush) {
		activeCanvas.freeDrawingBrush.color = drawingColorEl.value;
		activeCanvas.freeDrawingBrush.width = parseInt(drawingLineWidthEl.value, 10) || 1;
	}

	// TEXT OPTIONS
	addTextButton.onclick = function () {
		activeCanvas.add(new fabric.IText('Gib hier deinen Text ein.', {
			left: 50,
			top: 100,
			fontFamily: 'arial black',
			fill: '#333',
			fontSize: 50
		}));
	}

	$('text-color').onchange = function () {
		activeCanvas.getActiveObject().set("fill", this.value);
		activeCanvas.renderAll();
	};

	$('text-bg-color').onchange = function () {
		activeCanvas.getActiveObject().set("backgroundColor", this.value);
		activeCanvas.renderAll();
	};

	$('text-lines-bg-color').onchange = function () {
		activeCanvas.getActiveObject().set("textBackgroundColor", this.value);
		activeCanvas.renderAll();
	};

	$('text-stroke-color').onchange = function () {
		activeCanvas.getActiveObject().set("stroke", this.value);
		activeCanvas.renderAll();
	};

	$('text-stroke-width').onchange = function () {
		activeCanvas.getActiveObject().set("strokeWidth", this.value);
		activeCanvas.renderAll();
	};

	$('font-family-select').onchange = function () {
		activeCanvas.getActiveObject().set("fontFamily", this.value);
		activeCanvas.renderAll();
	};

	$('text-font-size').onchange = function () {
		activeCanvas.getActiveObject().set("fontSize", this.value);
		activeCanvas.renderAll();
	};


	radios5 = document.getElementsByName("fonttype");
	for (var i = 0, max = radios5.length; i < max; i++) {
		radios5[i].onclick = function () {

			if ($(this.id).checked == true) {
				if (this.id == "text-cmd-bold") {
					activeCanvas.getActiveObject().set("fontWeight", "bold");
				}
				if (this.id == "text-cmd-italic") {
					activeCanvas.getActiveObject().set("fontStyle", "italic");
				}

			} else {
				if (this.id == "text-cmd-bold") {
					activeCanvas.getActiveObject().set("fontWeight", "");
				}
				if (this.id == "text-cmd-italic") {
					activeCanvas.getActiveObject().set("fontStyle", "");
				}

			}


			activeCanvas.renderAll();
		}
	}
})();