
package enums;

public enum DisplayStyle {
BLOCK("display:block"), NONE ("display:none");

	private final String displayStyleToString;

	private DisplayStyle(String s){
		displayStyleToString = s;
	}

	public String getDisplayStyleString(){
		return displayStyleToString;
	}
}
