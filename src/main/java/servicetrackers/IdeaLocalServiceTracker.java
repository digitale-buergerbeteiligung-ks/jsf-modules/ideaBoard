package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import ideaService.service.IdeasLocalService;

public class IdeaLocalServiceTracker extends ServiceTracker<IdeasLocalService, IdeasLocalService> {

    public IdeaLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, IdeasLocalService.class, null);
    }
}
