package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import com.liferay.portal.kernel.service.RoleLocalService;

public class RoleLocalServiceTracker extends ServiceTracker<RoleLocalService, RoleLocalService> {
    public RoleLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, RoleLocalService.class, null);
    }
}

