package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import com.liferay.document.library.kernel.service.DLFolderLocalService;

public class DLFolderLocalServiceTracker extends ServiceTracker<DLFolderLocalService, DLFolderLocalService> {
    public DLFolderLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, DLFolderLocalService.class, null);
    }
}
