package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import analysisService.service.TagDataLocalService;

public class TagLocalServiceTracker extends ServiceTracker<TagDataLocalService, TagDataLocalService>{
    public TagLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, TagDataLocalService.class, null);
    }
}

